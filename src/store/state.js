export default {
    logined: false,
    customersList: [
        {
            id: 1,
            firstName: 'Maksym',
            lastName: 'Mazur',
            email: 'mazur@mazur.com',
            birthday: 409896000000,
            profession: 'front-end developer',
            rating: 10
        },
        {
            id: 2,
            firstName: 'Silvester',
            lastName: 'Stalone',
            email: 'Silvester@mazur.com',
            birthday: 502815060000,
            profession: 'Actor',
            rating: 8
        },
        {
            id: 3,
            firstName: 'Yuri',
            lastName: 'Gagarin',
            email: 'gsgsrin@mazur.com',
            birthday: 1185127200000,
            profession: 'Kosmonavt',
            rating: 9
        }
    ],
    users: [
        {
            id: 1,
            email: 'bill@microsoft.com',
            password: '12345678',
            firstName: 'Bill',
            lastName: 'Gates'
        },
        {
            id: 2,
            email: 'stive@apple.com',
            password: '1234567515',
            firstName: 'Stive',
            lastName: 'Jobs'
        },
        {
            id: 3,
            email: 'chak@noris.com',
            password: '11111111111',
            firstName: 'Chak',
            lastName: 'Noris'
        },{
            id: 4,
            email: 'cat@barsik.com',
            password: 'myau123123',
            firstName: 'Barsik',
            lastName: 'Cat'
        },
    ]


}