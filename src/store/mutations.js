export default {
  setLogined(state, payload) {
    state.logined = payload;
  },
  setEditCustomer(state, payload) {
    state.customersList.map((item, i) => {
      if (item.id === payload.id ) {
        state.customersList[i] = payload;
      }
    });
  },
  addCustomer(state, payload) {
    state.customersList.unshift(payload)
  },
  deleteCustomer(state, payload) {
    state.customersList.map((item, i) => {
      if (item.id === payload ) {
        state.customersList.splice(i, 1);
      }
    });
  }
}