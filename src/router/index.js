import Vue       from 'vue'
import VueRouter from 'vue-router'
import Home      from '@/views/index/'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'index',
    component: Home
  },
  {
    path: '/add',
    name: 'add',
    component: () => {
      return import('@/views/add/')
    }
  },
  {
    path: '/edit/:id',
    name: 'edit',
    component: () => {
      return import('@/views/edit/')
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
